const express = require('express')
const app = express()
const bodyParser = require('body-parser')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())


app.use(require("./routs/ejemplo1.js"))

let puerto=3000;
app.listen(puerto, function(){
  console.log("servidor iniciado en el puerto" + puerto)
})